#! /bin/sh

npm -g install \
    hotnode \
    bower \
    jshint \
    grunt \
    mocha \
    forever \
    less \
    coffee-script \
    coffeelint \
    typescript \
    tsi \
    yo \
    generator-webapp \
    generator-angular \
    generator-karma \
;
